const mysql = require("mysql");

//Connexion au server SQL
const connection = mysql.createConnection({
  multipleStatements: true,
  host: 'localhost',
  database: 'tp2',
  user: 'root',
  password: ''
});

connection.connect((err) => {
  if (err) throw err;
  console.log('Connecté à mysql');
});

module.exports = {
    connection
};
