const { argv, CMD_ADD_CLASS, CMD_ADD_TEACHER } = require("./args");
const { addClassCmd } = require("./commands/add-class");
const { addTeacherCmd } = require("./commands/add-teacher");
const { connection } = require("./db/connection");

(async () => {
    try {
        switch (argv._[0]) {
            case CMD_ADD_CLASS:
            await addClassCmd(argv, connection);
            break;
            case CMD_ADD_TEACHER:
            await addTeacherCmd(argv, connection);
            break;
        }
    } catch (e) {
        throw e;
        // process.exit(1);
    }
})();

// start the server
require("./server");