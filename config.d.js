const SQL_CONFIG = {
    multipleStatements: true,
    host: 'localhost',
    database: 'DATABASE_NAME',
    user: 'root',
    password: ''
};

const SERVER_PORT = 3000;

module.exports = {
    SQL_CONFIG,
    SERVER_PORT
};