const { generatePassword } = require("../utils/passwords");

function addTeacherCmd(argv, connection) {
    const firstname = argv.prenom;
    const lastname = argv.nom;
    const email = argv.email;
    const password = generatePassword();

    const sql = `INSERT INTO teachers (firstname, lastname, email, password)
        VALUES (?, ?, ?, ?)`;
    const values = [firstname, lastname, email, password];

    return new Promise((resolve, reject) => {
        connection.query(sql, values, (err, results) => {
            if (err) return reject(err);
            console.log("Mot de passe: " + password);
            resolve(results.insertId);
        });
    });
}

module.exports = {
    addTeacherCmd
};