const { parseStudentsFile } = require("../utils/students-csv-parser");

async function addClassCmd(argv, connection) {
    const className = argv.nom;
    const file = argv.fichier;

    return parseStudentsFile(file)
        .catch(err => console.error("Impossible de parser le fichier " + file))
        .then(students => {
            return createClass(className, connection)
                .then(classId => createStudents(students, classId, connection));
        });
}

function createClass(className, connection) {
    return new Promise((resolve, reject) => {
        const sql = "INSERT INTO classes (name) VALUES (?)";
        connection.query(sql, [className], (err, results) => {
            if (err) return reject(err);
            resolve(results.insertId);
        });
    })
}

function createStudents(students, classId, connection) {
    return new Promise((resolve, reject) => {
        let sql = `INSERT INTO students (firstname, lastname, class_id)
            VALUES `;
        const values = [];

        // build SQL query for each student
        students.forEach((student, i) => {
            sql += `(?, ?, ?)`;
            // add a comma if it's not the last student
            if (i + 1 < students.length) {
                sql += ', ';
            }

            values.push(student.firstname, student.lastname, classId);
        });

        connection.query(sql, values, (err, results) => {
            if (err) return reject(err);
            resolve();
        })
    });
}

module.exports = {
    addClassCmd
};