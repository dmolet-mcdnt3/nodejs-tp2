const express = require("express");
const bodyParser = require('body-parser');
const { SERVER_PORT } = require("./config");
const { connection } = require("./db/connection");

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get("/students", (req, res) => {
    const classFilter = req.query.class;
    let sql = "SELECT id, firstname, lastname, class_id FROM students";
    let values;
    if (classFilter) {
        sql += " WHERE class_id = ?";
        values = [classFilter];
    }

    connection.query(sql, values, (err, results) => {
        if (err) return res.status(500).send();
        res.json(results);
    });
});

app.get("/students/:id", (req, res) => {
    const sql = "SELECT id, firstname, lastname, class_id FROM students WHERE id = ?";
    connection.query(sql, [req.params.id], (err, results) => {
        if (err) return res.status(500).send();
        if (results.length == 0) return res.status(404).send();
        res.json(results[0]);
    });
});

app.get("/teachers", (req, res) => {
    const sql = "SELECT id, firstname, lastname, email FROM teachers";
    connection.query(sql, (err, results) => {
        if (err) return res.status(500).send();
        res.json(results);
    });
});

app.get("/teachers/:id", (req, res) => {
    const sql = "SELECT id, firstname, lastname, email FROM teachers WHERE id = ?";
    connection.query(sql, [req.params.id], (err, results) => {
        if (err) return res.status(500).send();
        if (results.length == 0) return res.status(404).send();
        res.json(results[0]);
    });
});

app.listen(SERVER_PORT, () => {
  console.log(`Serveur écoutant le port ${SERVER_PORT}...`);
});