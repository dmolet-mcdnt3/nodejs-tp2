const fs = require("fs");

function parseStudentsFile(file) {
    return new Promise((resolve, reject) => {
        fs.readFile(file, "utf8", (err, buffer) => {
            if (err) return reject(err);

            const lines = buffer.split(/\r?\n/);
            const students = lines.reduce((result, line) => {
                if (!line) return result;

                const names = line.split(/,\s*/);
                const student = {
                    firstname: names[0].trim(),
                    lastname: names[1].trim()
                };

                result.push(student);
                return result;
            }, []);

            resolve(students);
        });
    });
}

module.exports = {
    parseStudentsFile
};