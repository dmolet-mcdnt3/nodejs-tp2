const bcrypt = require('bcryptjs');

function hashPassword() {
    return new Promise((resolve, reject) => {
        bcrypt.genSalt(8, (err, salt) => {
            if (err) return reject(err);
            bcrypt.hash(password, salt, (err, hash) => {
                if (err) return reject(err);
                resolve(hash);
            })
        });
    })
}

function generatePassword() {
    return Math.floor(Math.random() * 1000);
}

module.exports = {
    hashPassword,
    generatePassword
};