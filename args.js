const yargs = require("yargs");

const classNameOptions = {
    describe: "Nom de la classe",
    required: true
};

const fileOptions = {
    describe: "Fichier à importer",
    required: true
};

const firstNameOptions = {
    describe: "Prénom du professeur",
    required: true
};

const lastNameOptions = {
    describe: 'Nom du professeur',
    required: true
};

const emailOptions = {
    describe: "Email du professeur",
    required: true
};

const CMD_ADD_CLASS = "ajoutClasse";
const CMD_ADD_TEACHER = "ajoutProf";

const argv = yargs
    .command(CMD_ADD_CLASS, "Ajouter une classe", {
        nom: classNameOptions,
        fichier: fileOptions
    })
    .command(CMD_ADD_TEACHER, "Ajouter un professeur", {
        prenom: firstNameOptions,
        nom: lastNameOptions,
        email: emailOptions
    })
    .help()
    .argv;

module.exports = {
    argv,
    CMD_ADD_CLASS,
    CMD_ADD_TEACHER
};